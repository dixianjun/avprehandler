# -*- coding:utf-8 -*-

import os
import multiprocessing
import time
import shutil 
from PIL import Image
from tools import drawFaces
from tools import detectFaces
from tools import show
from tools import close_show

AV_STAR_PATH = '/home/yueguang/AvStars'
#AV_STAR_PATH = '/home/yueguang/AvStars/まゆみ'
#AV_STAR_PATH = '/home/yueguang/AvStars/めぐみ'
#AV_STAR_PATH = '/home/yueguang/AvStars/佐野ともか'

SINGLE_STAR_AV_PATH = '/home/yueguang/SingleStarAvs/'

TEMP_PATH = 'temp/'

IS_SHOW_SINGLE_FACE = False

current_info = {}

def start(rootDir, result):
	for lists in os.listdir(rootDir): 
		img_path = os.path.join(rootDir, lists)
		if not os.path.isdir(img_path): 
			if img_path.endswith('.jpg') and '-' in img_path:
				star = os.path.basename(os.path.dirname(img_path))
				code = lists.split('.')[0]
				if current_info[code]:
					img = Image.open(img_path)
					x, y =  img.size
					if x == 800 or x == 774:
						av_image = SINGLE_STAR_AV_PATH + star + '_' + code + '.jpg'
						img.crop((420, 0, 800, y)).save(av_image)
						faces = detectFaces(av_image)
						if len(faces) == 1:
							if faces[0][2] - faces[0][0] > 60:
								print '面部数量1:' + img_path + ' face'
								result.write(img_path + '\n')
								
								if IS_SHOW_SINGLE_FACE:
									print '显示图片路径:' + img_path
									show_temp_path = TEMP_PATH + 'temp_show_' + str(time.time()) + '.jpg'
									drawFaces(av_image, faces, show_temp_path, (255, 0, 0))
									show(show_temp_path)
									time.sleep(2)
									close_show()
									os.remove(show_temp_path)
						else:
							print '面部数量:' + str(len(faces)) + ' 路径:' + img_path
							os.remove(av_image)
					else:
						print '非标准分辨率:' + img_path
				else:
					print '合集不处理:' + img_path
		else:
			star_path = img_path
			try:
				load_av_info(star_path)
				start(star_path, result)
			except Exception as e:
				with open("error.txt","a") as f:
					f.write(star_path)
					f.write(str(e))
					f.write("\n")

def is_single_star(av_class, av_star, star_name):
	is_single = True
	if '合集' in av_class:
		is_single = False
	else:
		if av_star is not None:
		 	if len(av_star.replace(star_name, '').replace(' ', '')) > 0:
				is_single = False
	return is_single
			
def load_av_info(path):
	star_name = path.split('/')[-1]
	global current_info
	current_info = {}
	info_file = path + '/' + star_name + '.txt'
	with open(info_file, 'r') as info_file:
		code = None
		av_class = None
		av_star = None
		for info in info_file:
			if '番号:' in info:
				if code is not None:
					if is_single_star(av_class, av_star, star_name):
						current_info[code] = True
					else:
						current_info[code] = False
				code = info.split(':')[-1].replace('\n', '')
				av_class = None
				av_star = None
			elif '类别:' in info:
				av_class = info.split(':')[-1].replace('\n', '')
			elif '演员:' in info:
				av_star = info.split(':')[-1].replace('\n', '')
		if code is not None:
			if is_single_star(av_class, av_star, star_name):
				current_info[code] = True
			else:
				current_info[code] = False

def init_temp():
	if os.path.exists(TEMP_PATH):
		shutil.rmtree(TEMP_PATH)
	os.makedirs(TEMP_PATH)
	if os.path.exists(SINGLE_STAR_AV_PATH):
		shutil.rmtree(SINGLE_STAR_AV_PATH)
	os.makedirs(SINGLE_STAR_AV_PATH)

if __name__ == '__main__':
	#load_av_info('/home/yueguang/AvStars/サヴァンナ サムソン')
	init_temp()
	with open('single_face.txt', 'w') as result:
		start(AV_STAR_PATH, result)
