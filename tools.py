# -*- coding:utf-8 -*-

import cv2
import subprocess
from PIL import Image
from PIL import ImageDraw

def drawFaces(image_path, faces, out_path, color):
	if faces:
		image = Image.open(image_path)
		draw_instance = ImageDraw.Draw(image)
		for (x1,y1,x2,y2) in faces:
		    draw_instance.rectangle((x1,y1,x2,y2), outline=color)
        	image.save(out_path)
	else:
		print '没有找到可识别面部'

def detectFaces(img_path):
    img = cv2.imread(img_path)
    face_cascade = cv2.CascadeClassifier("/usr/share/OpenCV/haarcascades/haarcascade_frontalface_alt.xml")
    #face_cascade = cv2.CascadeClassifier("/usr/share/OpenCV/haarcascades/haarcascade_frontalface_alt2.xml")
    #face_cascade = cv2.CascadeClassifier("/usr/share/OpenCV/haarcascades/haarcascade_frontalface_alt_tree.xml")
    #face_cascade = cv2.CascadeClassifier("/usr/share/OpenCV/haarcascades/haarcascade_frontalface_default.xml")
    if img.ndim == 3:
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    else:
        gray = img #if语句：如果img维度为3，说明不是灰度图，先转化为灰度图gray，如果不为3，也就是2，原图就是灰度图

    #faces = face_cascade.detectMultiScale(gray, 1.2, 5)#1.3和5是特征的最小、最大检测窗口，它改变检测结果也会改变
    faces = face_cascade.detectMultiScale(gray, 1.2, 5)#前一个值越小越容易找到更多脸，但是会出错，后一个越大找到脸越少
    result = []
    for (x,y,width,height) in faces:
        result.append((x,y,x+width,y+height))
    return result


def cv_show(img):
	cv2.namedWindow("Image") 
	cv2.imshow("Image", img)  
	cv2.waitKey (0)
	cv2.destroyAllWindows()

def show(img_path):
	subprocess.Popen(['eog', img_path]) 

def close_show():
	subprocess.Popen(['killall', 'eog']) 
